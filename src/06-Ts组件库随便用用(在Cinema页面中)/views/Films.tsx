import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'

interface fun1 {
  filmId: number,
  name: string,
  [propsName:string]:any
}

export default function Films(props:RouteComponentProps) {
  const [list, setlist] = useState([])
  useEffect(()=>{
    axios({
      url:'https://m.maizuo.com/gateway?cityId=440100&pageNum=1&pageSize=10&type=1&k=1057818',
      headers:{
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.0","e":"165294070798642513887233"}',
        'X-Host': 'mall.film-ticket.film.list'
      }
    }).then(res =>
      setlist(res.data.data.films))
  },[])
  return (
    <ul>
      {list.map((item:fun1) =>
        <li key={item.filmId} onClick={()=>{
          props.history.push(`/detail/${item.filmId}`)
        }}>{item.name}</li>
      )}
    </ul>
  )
}
