import React from 'react'
import { Button, Swiper, Space } from 'antd-mobile'
import { SwiperRef } from 'antd-mobile/es/components/swiper'
import { useRef } from 'react'

export default function Cinema() {
  const ref = useRef<SwiperRef>(null)
  return (
    <div>
      <Button color='primary'>Primary</Button>
      <Swiper style={{height:'12.5rem'}} allowTouchMove={true} ref={ref} loop>
        <Swiper.Item>111</Swiper.Item>
        <Swiper.Item>222</Swiper.Item>
        <Swiper.Item>333</Swiper.Item>
      </Swiper>
      <Space>
            <Button
              onClick={() => {
                ref.current?.swipePrev()
              }}
            >
              上一张
            </Button>
            <Button
              onClick={() => {
                ref.current?.swipeNext()
              }}
            >
              下一张
            </Button>
      </Space>
    </div>
  )
}
