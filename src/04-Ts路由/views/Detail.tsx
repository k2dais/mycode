import React from 'react'
import { RouteComponentProps } from 'react-router-dom'

interface myfil {
    id: string,
    [propsName:string]:any
  }

export default function Detail(props:RouteComponentProps<myfil>) {
  return (
    <div>
        {props.match.params.id}
    </div>
  )
}
