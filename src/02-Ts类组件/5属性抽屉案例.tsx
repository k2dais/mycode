import React, { Component } from 'react'
import Child from './Child'

export default class App extends Component {
  state ={
      type: true,
      text:'显示抽屉'
  }
  render() {
    return (
      <div>
        <Child type={this.state.type} text={this.state.text} gb={():void=>{
          this.setState({
            type: !this.state.type
          })
        }}></Child>
      </div>
    )
  }
}
