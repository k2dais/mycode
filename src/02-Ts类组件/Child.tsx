import React, { Component } from 'react'

interface cla {
    type: boolean,
    text: string,
    gb:()=>void
  }
  
 export default class Child extends Component<cla, any> {
    render() {
      return (
        <div>
          <button onClick={()=>{
            this.props.gb()
          }}>子组件的</button>
          {this.props.type?<span>{this.props.text}</span>:<span>没显示</span>}
        </div>
      )
    }
  }
  