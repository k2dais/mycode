import React, { Component } from 'react'

interface cla {
    text:string,
    textlist:string[]
}
export default class App extends Component<any,cla> {
    state = {
        text:'',
        textlist:[]
    }
    myref = React.createRef<HTMLInputElement>()    //要声明类型
  render() {
    return (
      <div>
        <input ref = {this.myref} onChange={()=>{
            this.setState({
                text:(this.myref.current as HTMLInputElement).value     //as 断言， 这里表示我断言this，myref.current一定是HTMLInputElement类型的值
            })
        }} value={this.state.text}></input>
        <button onClick={()=>{
            this.setState({
                textlist:[...this.state.textlist, (this.myref.current as HTMLInputElement).value],
                text:''
            })
        }}>点击加入战斗序列</button>
        <ul>
            {this.state.textlist.length ? this.state.textlist.map((item, index) =>
                <li>{item} <button onClick={()=>{
                    let newlist:string[] = [...this.state.textlist];
                    newlist.splice(index, 1)
                    this.setState({
                        textlist: newlist
                    })
                }}>退出战斗序列</button></li>):<span>空空如也</span>}
        </ul>
      </div>
    )
  }
}
