import React, { Component } from 'react'

interface cla {
  name:string,
  age: number
}

export default class App extends Component<any,cla> {     //<属性接口，状态接口>
  state = {
    name: 'miaohao',
    age: 23
  }
  render() {
    return (
      <div>
        <button onClick={()=>{
          this.setState({
            name:this.state.name.substring(0,1).toUpperCase()+this.state.name.substring(1)
          })
        }}>点击</button>
        {this.state.name}--{this.state.age}
      </div>
    )
  }
}
