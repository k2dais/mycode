import React, { Component } from 'react'

interface cla {
    name:string
}

export default class App extends Component {    
    state = {
        name:'miaohao',
        age: 25
    }
  render() {
    return (
      <div>
        <Child name={this.state.name}></Child>
      </div>
    )
  }
}

class Child extends Component<cla,any>{ //<属性接口，状态接口>
    render(){
        return(
           <div>
             {this.props.name}
           </div>
        )
    }
}