import React, { Component } from 'react'

interface cla {
    text:string,
    textlist:string[]
}

export default class App extends Component<any, cla> {
  state = {
    text:'',
    textlist:[]
  }
  render() {
    return (
      <div>
            <input onChange={(evt)=>{
                this.setState({
                    text:evt.target.value
                })
            }} value={this.state.text}></input>
            <button onClick={()=>{
                this.setState({
                    textlist:[...this.state.textlist, this.state.text],
                    text:''
                })
            }}>点击加入战斗序列</button>
            <ul>
                {this.state.textlist.length && this.state.textlist.map(item=>{
                    return(
                        <li>{item}</li>
                    )
                })}
                {!this.state.textlist.length && <li>空空如也</li>}
            </ul>
      </div>
    )
  }
}
