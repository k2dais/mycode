import React from 'react'

export default function App() {
  let a:string = 'miaohao';    //变量名:变量类型
  let b:boolean|number = 123   //变量名: 变量类型1|变量类型2
  let shuzu1:Array<string> = ['111', '222', '333']   //数组名:Array<数组类型>
  let shuzu2:Array<string|number> = ['111', 256, '333']  //数组名:Array<数组类型1|数组类型2>
  interface obj {  //类接口，定义一个类时要参照某一接口
    name:string,   //用到该接口的类必须有一个key名为name,其value必须为string类型
    age:number,    //用到该接口的类必须有一个key名为age,其value必须为number类型
    anyone?:boolean,   //用到该接口的类不是必须有一个key名为anyone,但如果用到其value必须为boolean类型
    [propsName:string]:any   //用到该接口的类的其他key对应的value类型随便
  }
  let obj1:obj = {
    name:'miaohao',
    age:25
  }
  let obj2:obj = {
    name:'fatter',
    age:26,
    anyone:true,
    kiss: 'nihao'
  }
  return (
    <div>
      {a}-{b}-{shuzu1}-{shuzu2}-{obj1.name}-{obj2.anyone}
    </div>
  )
}
