
export default function App() {
  interface obj{
    name:string,
    age:number,
    gender?:string,
    printName:(name:string,age:number)=>string   //接口对于函数的定义方式     函数名:(参数1：参数1类型，... , 参数n:参数n类型) => 函数返回值类型
  }
  
  let obj1:obj = {
    name:'miaohao',
    age:25,
    gender:'男',
    printName:(myname, myage) =>{      //函数的定义
      return myname + '+' + myage;
    }
  }

  let printgender = (gender:string):string =>{    //单独使用函数
    return gender;
  }


  let str:string =  obj1.printName(obj1.name,25)    //函数的调用

  return (
    <div>
      {str} -- {obj1.name}
      {printgender("男")}
    </div>
  )
}
