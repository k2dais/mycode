import React from 'react'

export default function App() {
  class A {
        protected name:string = 'miaohao';    //protected只能在自己和子类中使用
        private age:number = 25;   //peivate只能在自己中使用
        public gender:string = '男';   //public可以在类外进行访问
        public printname():number{
            return this.age;
        }
        
    };
    class B extends A {
        public printBinfo():string{
           return this.name;
        }
        
    };
    var c = new A();
    var d = new B();
    console.log(c.gender, c.printname(), d.printBinfo())
  return (
    <div>
        
    </div>
  )
}
