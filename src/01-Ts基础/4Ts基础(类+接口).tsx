import React from 'react'

export default function App() {
    interface cla {
        getName:()=>string
    }

    class A {
        nagu = ():string=>{
            return 'haha'
        };
        getName = ():string =>{
            return 'aaa'
        }
    }
    class B {
        getName = ():string =>{
            return 'bbb'
        }
    }
    class C implements cla{    //对类加上implements表示定义该类时候必须满足对应的接口。格式： 类名 implements 接口名
        getName = ():string =>{
            return 'ccc'
        }
    }
    function init(obj:cla):void{     //若想使用init函数，则参数的数据类型必须满足接口cla规定的规则，即“必须要有函数getName且返回值为string”。
        console.log(obj.getName);
    }
    let Al = new A();
    let Bl = new B();
    let Cl = new C()
    init(Al);
    init(Bl);
    init(Cl);
  return (
    <div>
        
    </div>
  )
}
