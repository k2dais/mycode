import React, { useState } from 'react'

export default function App() {
  const [name, setname] = useState<string>('miaohao');    //usestate<类型>
  return (
    <div>
        <button onClick={()=>{
            setname(name.substring(0,1).toUpperCase()+name.substring(1));
        }}></button>
        {name}
    </div>
  )
}
