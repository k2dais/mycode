import React, { useState } from 'react'

export default function App() {
  const [text,settext] = useState<string>('');
  const [textlist, settextlist] = useState<string[]>([]);
  const myref = React.createRef<HTMLInputElement>()
  return (
    <div>
        <input onChange={()=>{
            settext((myref.current as HTMLInputElement).value);
        }} ref={myref} value = {text}/>
        <button onClick={()=>{
            if(text.length){
                settextlist([...textlist, text])
                settext('')
            }
        }}>加入战斗序列</button>
        <ul>
            {textlist.length? textlist.map((item, index) =>
            <li>{item} <button onClick={()=>{
                let newlist = [...textlist];
                newlist.splice(index, 1);
                settextlist(newlist);
            }}>退出战斗序列</button></li>):<span>空空如也</span>}
        </ul>
    </div>
  )
}
