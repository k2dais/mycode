import React, { useState } from 'react'

export default function App() {
  const [name] = useState('miaohao')
  return (
    <div>
        <Child name={name}></Child>
    </div>
  )
}

interface fun {     //与类组件一样，函数式组件要写一个接口
    name:string
}

function Child(props:fun){   //子函数组件形参直接 (props:接口名)即可
    let {name} = props
    return (
        <div>
            {name}
        </div>
    )
}