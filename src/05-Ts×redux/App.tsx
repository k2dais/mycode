import React, { useEffect, useState } from 'react';
import store from './redux/store';
import Myrouter from './routerindex/index'

function App() {
  const [isShow, setisShow] = useState(store.getState().isShow)
  useEffect(()=>{
    store.subscribe(()=>{
      setisShow(store.getState().isShow)
    })
  },[setisShow])
  return (
    <div>
        <Myrouter></Myrouter>
        {isShow && 
        <ul>
          <li>电影</li>
          <li>影院</li>
          <li>我的</li>
        </ul>}
    </div>
  );
}

export default App;
