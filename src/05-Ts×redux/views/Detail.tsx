import React from 'react'
import { useEffect } from 'react'
import store from '../redux/store'
import { RouteComponentProps } from 'react-router-dom'

interface myfil {
    id: string,
  }

export default function Detail(props:RouteComponentProps<myfil>) {
  useEffect(()=>{
    store.dispatch({
      type:'hide',
    })
    return ()=>{
      store.dispatch({
        type:'show'
      })
    }
  })
  return (
    <div>
        {props.match.params.id}
    </div>
  )
}
