import { createStore } from "redux";

interface IOld {
    isShow:boolean
}

interface IAction {
    type:string,
    value?:any
}

const reducer = (oldstate:IOld={isShow:true},action:IAction) =>{
    let newstate:IOld = {...oldstate}
    switch(action.type){
        case 'show':
            newstate.isShow = true;
            return newstate;
        case 'hide':
            newstate.isShow = false;
            return newstate;
        default:
            return oldstate;
    }
}

const store = createStore(reducer);

export default store;