import React, { Component } from 'react'
import { HashRouter,Switch,Route, Redirect } from 'react-router-dom'
import Center from '../views/Center'
import Cinema from '../views/Cinema'
import Detail from '../views/Detail'
import Films from '../views/Films'

export default class index extends Component {
  render() {
    return (
      <div>
        <HashRouter>
            <Switch>
                <Route path='/films' component={Films}></Route>
                <Route path='/cinemas' component={Cinema}></Route>
                <Route path='/center' component={Center}></Route>
                <Route path='/detail/:id' component={Detail}></Route>
                <Redirect from='/' to='/films'></Redirect>
            </Switch>
        </HashRouter>
      </div>
    )
  }
}
